# jDrawingLib ![](https://gitlab.com/riddler_arg/jdrawinglib/badges/master/build.svg) ![coverage](https://gitlab.com/riddler_arg/jdrawinglib/badges/master/coverage.svg)

**jDrawingLib** is a small library that allows easy object oriented drawing.

## Building From Source

1. Install [Ant]
1. Checkout the code at Github `git clone https://gitlab.com/riddler_arg/jdrawinglib.git`
1. Open the folder `cd jdrawinglib`
1. Package it with ant `ant build-all`
1. Run some examples:

```
cd dist
java -jar jdrawinglib-0.4.2.jar
java -jar jdrawinglib-0.4.2.jar Example01
```

[Ant]: http://ant.apache.org/manual/install.html

