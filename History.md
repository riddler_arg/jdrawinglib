
0.4.2 / 2018-07-14
==================

  * Add LICENSE
  * Merge branch 'gitHub-ci' into 'master'
  * Merge branch 'add-misc-tests' into 'master'
  * Hating this...
  * Change compiler version
  * Attemt to make Gitlab/CI work
  * Don't specify Javac Version
  * Remove Jacoco
  * Add .gitlab-ci.yml
  * Fix visibility
  * Merge branch 'master' of gitlab.com:riddler_arg/jdrawinglib
  * Update .gitignore
  * Add some more JavaDocs
  * Add a lot of JavaDocs
  * Make method private
  * Update README.md
  * Add tests for previous commit
  * Take visibility into account in equals and hashcode
  * Remove unused methods
  * Extract variable from for
  * Merge pull request #18 from RiddlerArgentina/test-gpath
  * Merge pull request #19 from RiddlerArgentina/test-goval
  * Add GLine Tests
  * Add GOval test
  * Add clone test for GPoly
  * Add GPath tests
  * Remove trailing spaces
  * Merge pull request #17 from RiddlerArgentina/test-GPoly
  * Merge pull request #16 from RiddlerArgentina/remove-dead-code
  * Add GPoly Tests
  * Remove dead code and inline cross and dot
  * Merge pull request #15 from RiddlerArgentina/gregpoly-tests
  * Create Tests for GRegPoly
  * Use a better method for contains triangle
  * Use formula in GRegPoly area and perimeter
