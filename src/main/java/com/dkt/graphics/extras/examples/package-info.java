/**
 * Contains some examples that can be executed from the CLI.
 * <pre>
 *    java -jar jdrawinglib.jar Example01
 * </pre>
 */
package com.dkt.graphics.extras.examples;
