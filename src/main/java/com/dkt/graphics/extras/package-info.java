/**
 * Contains some extra elements, they are a bit more complex than the the ones
 * in {@link com.dkt.graphics.elements}.
 * Some of the most notable additions are:
 * <ul>
 *   <li>{@link com.dkt.graphics.extras.GAxis}: Creates an axis to plot
 *       behind equations</li>
 *   <li>{@link com.dkt.graphics.extras.GGrid}: Which creates a monospaced line
 *       grid.</li>
 *   <li>{@link com.dkt.graphics.extras.GTransform}: Which wraps an affine
 *       transform and is used to rotate, scale and sheer graphic elements</li>
 *   <li>{@link com.dkt.graphics.extras.GClip}: Which wraps an awt clip used to
 *       trim graphical elements</li>
 * </ul>
 */
package com.dkt.graphics.extras;
