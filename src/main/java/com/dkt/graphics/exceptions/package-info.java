/**
 * Contains exceptions thrown by {@code jdrawinglib}.
 */
package com.dkt.graphics.exceptions;
